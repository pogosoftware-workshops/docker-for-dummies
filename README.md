# Docker For Dummies #

Contains sample application and infrastructure.

Requirements is Linux Machine (virtual or main operation system) with installed Docker >= v17.12.1-ce and Docker Compose >= v1.19.0. You can create it yourself or you can use prepared one for.

# Manual installation

Instruction how to install Docker and Docker Compose on Linux or Mac you can find on [Docker](https://www.docker.com/community-edition) and [Docker-Compose](https://docs.docker.com/compose/install/) site.
# Lab Setup

## Requirements:
* Windows 10
* Vagrant (>=2.0.2) https://www.vagrantup.com/downloads.html
* VirtualBox (>=5.2.8) https://www.virtualbox.org/wiki/Downloads
* Cmder http://cmder.net/
* Git
* Chrome or Firefox Browser

## How to start?
1. Open cmd (or if you are using cmder) and type

```
git clone https://bitbucket.org/pogosoftware-workshops/docker-for-dummies
```

2. Go to the Docker For Dummy folder

```
cd docker-for-dummies
```

3. Setup virtual machine (this command will use Vagrant to create and run virtual machine)

```
vagrant up
```

4. Login to the VM (type exit and hit 'Enter' to exit)

```
vagrant ssh
```

5. Gracefully close VM

```
vagrant halt
```

If you will NEVER again use this virtual machine then type:

```
vagrant destroy
```

# Troubleshooting:

## Uninstall Hyper-V

If you have installed Hyper-V then VirtualBox will not be working properly. First you need uninstall or disabled Hyper-V.