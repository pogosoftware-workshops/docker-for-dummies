using AutoMapper;
using Roknoe.Api.Models.Inbound;
using Roknoe.Common.Messages;
using Roknoe.Common.RabbitMq;

namespace Roknoe.Api.MessageFormatters
{
    public class ParticipantWasSignUpMessageFormatter : IMessageFormatter<SignUpForEvent, ParticipantWasSignUpMessage>
    {
        private readonly IMapper _mapper;

        public ParticipantWasSignUpMessageFormatter(IMapper mapper)
        {
            _mapper = mapper;    
        }

        public ParticipantWasSignUpMessage Format(SignUpForEvent participant)
        {
            return _mapper.Map<ParticipantWasSignUpMessage>(participant);
        }   
    }
}