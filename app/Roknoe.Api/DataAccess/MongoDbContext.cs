using System;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Roknoe.Api.DataAccess.DbModels;

namespace Roknoe.Api.DataAccess
{
    public class MongoDbContext : IMongoDbContext
    {
        private IMongoDatabase _database { get; }

        public MongoDbContext(IOptions<MongoDbConfiguration> optionAccessor) 
        {
            var configuration = optionAccessor.Value;

            try 
            { 
                var mongoClient = new MongoClient(new MongoUrl(configuration.ConnectionString)); 
                _database = mongoClient.GetDatabase(configuration.DatabaseName); 
            } 
            catch (Exception ex) 
            { 
                throw new Exception("Can not access to db server.", ex); 
            } 
        }

        public IMongoCollection<Event> Events => _database.GetCollection<Event>("Events");
    }
}