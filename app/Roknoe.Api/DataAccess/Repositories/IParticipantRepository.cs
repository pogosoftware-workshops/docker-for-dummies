using System;
using System.Threading.Tasks;
using Roknoe.Api.Models;
using Roknoe.Api.DataAccess.DbModels;
using System.Collections.Generic;
using System.Threading;

namespace Roknoe.Api.DataAccess.Repositories
{
    public interface IParticipantRepository
    {
        Task<EventDetails> SignUpForEventAsync(Guid eventId, Participant participant, CancellationToken cancellationToken = default(CancellationToken));
        Task<ICollection<Participant>> GetEventParticipantsAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));
        Task QuitEventAsync(Guid eventId, string name, CancellationToken cancellationToken = default(CancellationToken));
    }
}
