using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.DataAccess.DbModels;

namespace Roknoe.Api.DataAccess.Repositories
{
    public interface IEventRepository
    {
        Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken));

        Task<Event> GetEventAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));

        Task AddNewEventAsync(Event eventToAdd, CancellationToken cancellationToken = default(CancellationToken));
    }
}
