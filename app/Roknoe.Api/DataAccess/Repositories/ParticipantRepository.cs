using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Roknoe.Api.Models;
using Roknoe.Api.DataAccess.DbModels;
using System.Threading;
using System.Linq;

namespace Roknoe.Api.DataAccess.Repositories
{
    public class ParticipantRepository : IParticipantRepository
    {
        private readonly IMongoDbContext _context;

        public ParticipantRepository(IMongoDbContext context)
        {
            _context = context;
        }

        public async Task<EventDetails> SignUpForEventAsync(Guid eventId, Participant participant, CancellationToken cancellationToken = default(CancellationToken))
        {
            var signUpEvent = await _context.Events
                .AsQueryable()
                .Where(x => x.Id.Equals(eventId))
                .SingleOrDefaultAsync(cancellationToken);
            signUpEvent.Participants.Add(participant);

            var filter = Builders<Event>.Filter.Eq(x => x.Id, eventId);
            var update = Builders<Event>.Update.Set(x => x.Participants, signUpEvent.Participants);
            await _context.Events.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);

            return new EventDetails(signUpEvent.EventName, signUpEvent.EventDate, signUpEvent.Place);
        }

        public async Task<ICollection<Participant>> GetEventParticipantsAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var oneEvent = await _context.Events
                .AsQueryable()
                .Where(x => x.Id.Equals(eventId))
                .SingleOrDefaultAsync(cancellationToken);

            return oneEvent.Participants;
        }

        public async Task QuitEventAsync(Guid eventId, string name, CancellationToken cancellationToken = default(CancellationToken))
        {
            var oneEvent = await _context.Events
                .AsQueryable()
                .Where(x => x.Id.Equals(eventId))
                .SingleOrDefaultAsync(cancellationToken);

            var participantsToRemove = oneEvent.Participants.Where(p => p.Name == name).ToList();
            foreach (var participant in participantsToRemove)
            {
                oneEvent.Participants.Remove(participant);
            }

            var filter = Builders<Event>.Filter.Eq(x => x.Id, eventId);
            var update = Builders<Event>.Update.Set(x => x.Participants, oneEvent.Participants);
            await _context.Events.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }
    }
}
