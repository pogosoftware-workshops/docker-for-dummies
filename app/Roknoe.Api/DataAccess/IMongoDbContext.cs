using MongoDB.Driver;
using Roknoe.Api.DataAccess.DbModels;

namespace Roknoe.Api.DataAccess
{
    public interface IMongoDbContext
    {
         IMongoCollection<Event> Events { get; }
    }
}