using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Roknoe.Api.DataAccess.Repositories;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Models.Outbound;

namespace Roknoe.Api.Services
{
    public class EventService : IEventService
    {
        private readonly IMapper _mapper;
        private readonly IEventRepository _eventRepository;

        public EventService(IMapper mapper, IEventRepository eventRepository)
        {
            _mapper = mapper;
            _eventRepository = eventRepository;
        }

        public async Task<ICollection<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var events = await _eventRepository.GetAllEventsAsync(cancellationToken);
            return _mapper.Map<ICollection<Event>>(events);
        }

        public async Task<Event> GetEventAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var singleEvent = await _eventRepository.GetEventAsync(eventId, cancellationToken);
            return _mapper.Map<Event>(singleEvent);
        }

        public async Task<Event> AddNewEventAsync(AddEvent request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var eventToAdd = _mapper.Map<DataAccess.DbModels.Event>(request);
            await _eventRepository.AddNewEventAsync(eventToAdd, cancellationToken);

            return _mapper.Map<Event>(eventToAdd);
        }
    }
}
