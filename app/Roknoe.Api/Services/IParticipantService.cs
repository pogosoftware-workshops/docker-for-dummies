using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Models.Outbound;

namespace Roknoe.Api.Services
{
    public interface IParticipantService
    {
        Task SignUpForEventAsync(Guid eventId, SignUpForEvent request, CancellationToken cancellationToken = default(CancellationToken));
        Task<ICollection<Participant>> GetEventParticipantsAsync(Guid eventId, CancellationToken cancellationToken = default(CancellationToken));
        Task QuitEventAsync(Guid eventId, string name, CancellationToken cancellationToken = default(CancellationToken));
    }
}
