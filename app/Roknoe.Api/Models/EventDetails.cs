using System;

namespace Roknoe.Api.Models
{
    public class EventDetails
    {
        public EventDetails(string eventName, DateTime eventDate, string place)
        {
            EventName = eventName;
            EventDate = eventDate;
            Place = place;
        }

        public string EventName { get; }

        public DateTime EventDate { get; }

        public string Place { get; }
    }
}
