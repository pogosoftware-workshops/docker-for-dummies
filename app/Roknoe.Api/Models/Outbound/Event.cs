using System;
using System.Collections.Generic;

namespace Roknoe.Api.Models.Outbound
{
    public class Event
    {
        public Guid EventId { get; set; }

        public string EventName { get; set; }

        public DateTime EventDate { get; set; }

        public DateTime EntriesTo { get; set; }

        public string Place { get; set; }

        public string Description { get; set; }

        public ICollection<Agenda> Agenda { get; set; }
    }
}
