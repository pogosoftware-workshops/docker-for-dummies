namespace Roknoe.Api.Models.Outbound
{
    public class Participant
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}