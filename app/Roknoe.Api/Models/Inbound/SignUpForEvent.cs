using System.ComponentModel.DataAnnotations;

namespace Roknoe.Api.Models.Inbound
{
    public class SignUpForEvent
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid.")]
        public string Email { get; set; }
    }
}
