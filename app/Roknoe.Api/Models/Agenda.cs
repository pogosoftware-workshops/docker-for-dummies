using System;

namespace Roknoe.Api.Models
{
    public class Agenda
    {
        public DateTime Timeline { get; set; }

        public string Title { get; set; }
    }
}