using AutoMapper;
using Roknoe.Api.Models;
using Roknoe.Api.DataAccess.DbModels;
using Roknoe.Api.Models.Inbound;
using Roknoe.Common.Messages;
using DbParticipant = Roknoe.Api.DataAccess.DbModels.Participant;

namespace Roknoe.Api.Profiles
{
    public class ParticipantProfile : Profile
    {
        public ParticipantProfile()
        {
            CreateMap<SignUpForEvent, DbParticipant>();
            CreateMap<Participant, ParticipantWasSignUpMessage>();
            CreateMap<EventDetails, ParticipantWasSignUpMessage>();
        }
    }
}
