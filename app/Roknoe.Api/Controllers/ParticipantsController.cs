using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roknoe.Api.Models.Inbound;
using Roknoe.Api.Services;

namespace Roknoe.Api.Controllers
{
    [Route("/api/events/{eventId}/participants")]
    [Produces("application/json")]
    public class ParticipantsController : Controller
    {
        private readonly IParticipantService _participantService;

        public ParticipantsController(IParticipantService participantService)
        {
            _participantService = participantService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignUpForEvent(Guid eventId, [FromBody] SignUpForEvent request)
        {
            await _participantService.SignUpForEventAsync(eventId, request);
            return NoContent();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetEventParticipants(Guid eventId) =>
            Ok(await _participantService.GetEventParticipantsAsync(eventId));

        [HttpDelete]
        [Route("{name}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> QuitEvent(Guid eventId, [Required] string name)
        {
            await _participantService.QuitEventAsync(eventId, name);
            return NoContent();
        }
    }
}
