using System.Linq;
using AutoMapper;

namespace Roknoe.Api.Extensions
{
    public static class AutoMapperExtensions
    {
        public static TResult MapInto<TResult>(this IMapper mapper, params object[] objects)
        {
            var res = mapper.Map<TResult>(objects.First());
            return objects.Skip(1).Aggregate(res, (r, obj) => mapper.Map(obj, r));
        }
    }
}