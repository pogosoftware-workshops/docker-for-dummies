import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { SignUpForEvent } from '@participant/models/signUpForEvent.interface';

@Injectable()
export class ParticipantService {
    constructor(private http: Http) { }

    getEventParticipants(eventId: string) : Observable<any> {
      return this.http.get(`/api/events/${eventId}/participants`)
        .map((res : Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }
  
    signUpForEvent(eventId : string, participant : SignUpForEvent) : Observable<any> {
        return this.http.post(`/api/events/${eventId}/participants`, participant);
    }
  
    quitEvent(eventId: string, name: string) : Observable<any> {
        return this.http.delete(`/api/events/${eventId}/participants/${name}`);
    }
}