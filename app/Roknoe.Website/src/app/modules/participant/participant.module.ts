import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// Components
import { ParticipantsComponent } from '@participant/components/participants/participants.component';

// Services
import { ParticipantService } from './participant.service';
import { SignUpForEventComponent } from '@participant/components/signUpForEvent/sign-up-for-event.component';

@NgModule({
    declarations: [ ParticipantsComponent, SignUpForEventComponent ],
    imports: [ CommonModule, HttpModule, FormsModule, ReactiveFormsModule, NgbModule ],
    exports: [ ParticipantsComponent, SignUpForEventComponent ],
    providers: [ ParticipantService ],
})
export class ParticipantModule {}