import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { NewEvent } from '@event/models/newEvent.interface';

@Injectable()
export class EventService {
    constructor(private http: Http) { }

    getAllEvents() : Observable<any> {
        return this.http.get(`/api/events`)
            .map((res : Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
    }

    addNewEvent(newEvent : NewEvent) : Observable<any> {
        return this.http.post(`/api/events`, newEvent);
    }
}