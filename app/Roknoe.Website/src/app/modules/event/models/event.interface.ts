export interface Event {
    eventId : string;
    eventName : string;
    eventDate : Date;
    place : string;
    description : string;
}
