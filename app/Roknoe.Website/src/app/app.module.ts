import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing } from './app.routing';

// Components
import { AppComponent } from './app.component';
import { TopMenuComponent } from './components/top-menu/top-menu.component';

// Modules
import { EventModule } from '@event/event.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    routing,
    EventModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
