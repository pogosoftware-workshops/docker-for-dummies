using System.Threading.Tasks;
using RawRabbit;

namespace Roknoe.Common.RabbitMq
{
    public class Publisher<TModel, TMessage> : IPublisher<TModel, TMessage>
    {
        private readonly IMessageFormatter<TModel, TMessage> _formatter;
        private readonly IBusClient _busClient;

        public Publisher(IBusClient busClient, IMessageFormatter<TModel, TMessage> formatter)
        {
            _busClient = busClient;
            _formatter = formatter;
        }

        public async Task PublishAsync(TModel model)
        {
            var message = _formatter.Format(model);
            await _busClient.PublishAsync(message);
        }
    }
}