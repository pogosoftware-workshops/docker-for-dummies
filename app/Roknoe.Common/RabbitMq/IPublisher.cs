using System.Threading.Tasks;

namespace Roknoe.Common.RabbitMq
{
    public interface IPublisher<TModel, TMessage>
    {
        Task PublishAsync(TModel Message);
    }
}