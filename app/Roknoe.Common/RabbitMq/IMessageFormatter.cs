namespace Roknoe.Common.RabbitMq
{
    public interface IMessageFormatter<TModel, TMessage>
    {
        TMessage Format(TModel model);
    }
}