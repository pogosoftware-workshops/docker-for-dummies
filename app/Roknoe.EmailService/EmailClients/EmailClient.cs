using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using Roknoe.EmailService.Configuration;

namespace Roknoe.EmailService.EmailClients
{
    public class EmailClient : IEmailClient
    {
        private readonly EmailConfiguration _configuration;

        public EmailClient(IOptions<EmailConfiguration> optionsAccessor)
        {
            _configuration = optionsAccessor.Value;
        }

        public async Task SendEmailAsync<TMessage>(TMessage message)
            where TMessage : MimeMessage
        {
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_configuration.Host, _configuration.Port, SecureSocketOptions.None).ConfigureAwait(false);
                await client.SendAsync(message).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
    }
}